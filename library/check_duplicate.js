const config = require('./config');


module.exports.check_duplicate = (async function (refno, PARTNER_ID, REFER_POINTBURN_TYPE, res) {
   var dt = new Date().toISOString();

        var dv = dt.substring(0, 10);

        var date_v = dv.substring(0, 4) + dv.substring(5, 7) + dv.substring(8, 10);

        //logger.debug('Date_V:', date_v);

        var tv = dt.substring(11, 19);
        var hp = tv.substring(0, 2);
        var mp = tv.substring(3, 5);
        var sp = tv.substring(6, 8);

        //console.log('Time-7:', hp);

        hp = parseInt(hp) + 7;

        //console.log('Time+7:', hp);

        if (hp > 23) {

                hp = hp - 24;

        }

        if (hp < 9) {

                hp = '0' + hp.toString();
        } else {

                hp = hp.toString();
        }

        var time_v = hp + mp + sp;
        //logger.debug('Time_V:', time_v);

        var dtf = date_v + time_v;
    const pool =  new require('node-jt400').pool(config);
    st = ("Select * From MBRFLIB/MCRR1P where  MBREFT='"+refno+"' and PNID = '"+ PARTNER_ID + "' and MBTYR = '" + REFER_POINTBURN_TYPE +"'");
    try {
        q3 = await pool.query(st);
        if ( q3.length > 0) {
                        res.status(200).send({
                "RESP_SYSCDE": 200,
                "RESP_DATETIME": dtf,
                "RESP_CDE": "303",
                "RESP_MSG": "Duplicate Reference No."
            });
            return await q3;
        } else {
            return q3;
        }
    } catch {
        res.status(200).send({
            "RESP_SYSCDE": 200,
            "RESP_DATETIME": dtf,
            "RESP_CDE": "304",
            "RESP_MSG": "Cannot connect Database"
        });
    }

})
