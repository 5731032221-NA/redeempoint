const express = require('express')
const app = express();
const isInteger = require("is-integer");
const bodyParser = require('body-parser');
const refno = require('./library/genrefno');
//const duplicate = require('./library/check_duplicate');
const config = require('./library/config');
const datetime = require('./library/datetime');
//const schema = require('./library/schema');
const checkPartnerNBR = require('./library/checkPartnerNBR');
const getMCard = require('./library/checkMCard');

app.listen(8121, function () {
	console.log('app listening on port 8121!');
})

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));

app.post('/redeem', async function (req, res) {
		// datetime
		var dtf = await datetime.getdatetime();
		
		
		// check valid
		if (typeof req.body.POINTBURN_TYPE == 'undefined' || typeof req.body.POINTBURN_TYPE != 'string') {
			console.log('TYPE : undefined');
			res.json({
					"RESP_SYSCDE": 200,
					"RESP_DATETIME": dtf,
					"RESP_CDE": 401,
					"RESP_MSG": "Missing Required Field"
			});
			return;
		} else if (req.body.POINTBURN_TYPE != 'CC' && req.body.POINTBURN_TYPE != 'DP' && req.body.POINTBURN_TYPE != 'MI' && req.body.POINTBURN_TYPE != 'PR' && req.body.POINTBURN_TYPE != 'SP') {
			console.log('TYPE : unknown');
			res.json({
					"RESP_SYSCDE": 200,
					"RESP_DATETIME": dtf,
					"RESP_CDE": 402,
					"RESP_MSG": "Invalid Format"
			});
			return;
		}
		
		// check schema
		//schema.checkSchema(req.body.POINTBURN_TYPE);
		
		// flow
		let checkPartner = await checkPartnerNBR.checkPartnerNBR(req,res);
		if(checkPartner == true){
			let MCard = await getMCard.checkMCard(req,res);
			//MCard = JSON.parse(MCard);
			var cal_POINTBURN = 0;
			if (req.body.POINTBURN_TYPE == "DP") {
					cal_POINTBURN = parseInt(req.body.POINTBURN_MPOINT);
			} else if (req.body.POINTBURN_TYPE == "MI") {
					cal_POINTBURN = parseInt(req.body.POINTBURN_MPOINT) * parseInt(req.body.POINTBURN_MILE);
			} else if (req.body.POINTBURN_TYPE == "CC") {
					cal_POINTBURN = parseInt(req.body.POINTBURN_MPOINT) * parseInt(req.body.POINTBURN_PIECE);
			} else if (req.body.POINTBURN_TYPE == "SP") {
					cal_POINTBURN = parseInt(req.body.POINTBURN_MPOINT) * parseInt(req.body.POINTBURN_PIECE);
			} else if (req.body.POINTBURN_TYPE == "PR") {
					cal_POINTBURN = parseInt(req.body.POINTBURN_MPOINT) * parseInt(req.body.POINTBURN_PIECE);

			} else {
					console.log('Unknown type');
					res.json({
							"RESP_SYSCDE": 200,
							"RESP_DATETIME": dtf,
							"RESP_CDE": 402,
							"RESP_MSG": "Invalid Format"
					});
					return;
			}

			if (parseInt(MCard[0].MBPOINT) < cal_POINTBURN) {
					res.json({
							"RESP_SYSCDE": 200,
							"RESP_DATETIME": dtf,
							"RESP_CDE": 201,
							"RESP_MSG": "Insufficient MPoint"
					});
					return;
			}
			console.log(req.body.POINTBURN_TYPE);
			console.log(MCard[0].MBPOINR);
			var cal_MPOINR = parseInt(MCard[0].MBPOINR) + cal_POINTBURN;
			console.log(cal_MPOINR);
			console.log(MCard[0].MBPOINC);
			var cal_MBPOINT = parseInt(MCard[0].MBPOINC) - cal_MPOINR;
			console.log(cal_MPOINR);
			console.log(cal_MBPOINT);
			console.log(MCard[0].MBCODE);
			
			// log
			var today = new Date();
			var date_str = await today.getFullYear().toString() + ((today.getMonth() + 1) < 10 ? '0' : '').toString() + (today.getMonth() + 1).toString() + (today.getDate() < 10 ? '0' : '').toString() + today.getDate();
			var date_str4 = date_str.substr(2, 4);
			var mtyr = await req.body.POINTBURN_TYPE;
			var mrec = mtyr + date_str4;
			console.log(mrec);
			var mrec_n = '';
			var mrec_c = '';
			try{
                                                                                        
				var rcp_result = await refno.genrefno("99",mtyr,res);
			 
				mrec_n = await rcp_result.Return_ReceiptNo;
				
				res.json({
							"RESP_SYSCDE": 200,
							"RESP_DATETIME": dtf,
							"RESP_CDE": 200,
							"RESP_MSG": "add log"
					});
				
			}
				catch(error){
					res.status(500);
					res.json({
						"RESP_SYSCDE": 200,
						"RESP_DATETIME": dtf,
						"RESP_CDE": 500,
						"RESP_MSG": "Log Failed : MCRTA3P",
						"MCARD_NUM": "",
						"CARD_TYPE": 0,
						"CARD_EXPIRY_DATE": "",
						"CARD_POINT_BALANCE": "",
						"CARD_POINT_EXPIRY": "",
						"CARD_POINT_EXP_DATE": "",
						"POINTBURN_MPOINT_SUCCESS": 0
					});
			}
			
		}else{
			console.log('No partner');
			res.json({
					"RESP_SYSCDE": 200,
					"RESP_DATETIME": dtf,
					"RESP_CDE": 301,
					"RESP_MSG": "Not success/ Not found Partner ID/Partner NBR",
					"MCARD_NUM": "",
					"CARD_TYPE": 0,
					"CARD_EXPIRY_DATE": "",
					"CARD_POINT_BALANCE": "",
					"CARD_POINT_EXPIRY": "",
					"CARD_POINT_EXP_DATE": "",
					"DEMO_TH_TITLE": "",
					"DEMO_TH_NAME": "",
					"DEMO_TH_SURNAME": "",
					"DEMO_EN_TITLE": "",
					"DEMO_EN_NAME": "",
					"DEMO_EN_SURNAME": ""
			});
			return;
		}
			
})